#ifndef __book_order__order__
#define __book_order__order__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct order {
    char *title;
    double price;
    char *idnum;
    char *category;
} order_t;

void order_destroy(order_t *);
order_t * order_create(char *, double, char *, char *);

#endif