#ifndef __book_order__library__
#define __book_order__library__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <ctype.h>
#include "customer.h"
#include "queue.h"
#include "order.h"

int get_categories(char *);
order_t *get_current_order();
void assign_categories_for_queues();
queue_t *get_queue_for_category(char *);
void set_isdone_private_globally();

/*printing out final report*/
void print_final_report();
void print_balance(customer_t *);
void print_suc_orders(struct suc_order *);
void print_failed_orders(struct fail_order *);

#endif
