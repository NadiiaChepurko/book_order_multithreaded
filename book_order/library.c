#include "library.h"

/*extern variables that we get from our source files*/
char *given_categories;
queue_t **global_array_of_queues;
int number_of_categories;
customer_t *global_customer_hash;

void assign_categories_for_queues(){
    char *temp = given_categories;
    char *buffer;
    char *buffer_ptr;
    queue_t *queue;
    int i = 0;
    while(i < number_of_categories){
        buffer = calloc(strlen(given_categories) + 1, 1);
        buffer_ptr = buffer;
        queue = global_array_of_queues[i];
        
        while(*temp && *temp != '\n'){
            *buffer_ptr++ = *temp++;
        }
        if(*temp == '\n')temp++;
        
        queue->category = calloc(strlen(buffer) + 1, 1);
        queue->category = strcpy(queue->category, buffer);
        free(buffer);
        i++;
    }
}

queue_t *get_queue_for_category(char *category){
    int i = 0;
    queue_t *temp;
    for(i = 0; i < number_of_categories; i++){
        temp = global_array_of_queues[i];
        if(strcmp(temp->category, category) == 0)
            return temp;
    }
    return NULL;
}

int get_categories(char *categories_file_name){
    int bufferCapacity = 1000;
    int currentLength = 0;
    given_categories = calloc(1000, 1);
    char *given_categoriesPtr = given_categories;
    int number_of_categories = 0;
    
    FILE *fp = fopen(categories_file_name, "r");
    if (fp == NULL) {
        fprintf(stderr, "An error occurred on line %d while opening the file %s.\n" ,__LINE__, __FILE__);
        exit(-1);
    }
    
    int c = 0;
    int escape_before = 0;
    
    while((c = getc(fp)) !=EOF){
        if(c != '\n' && c != '\r' && escape_before == 1){
            number_of_categories++;
            escape_before = 0;
        }
        if(currentLength + 1 == bufferCapacity){
            given_categories = realloc(given_categories, bufferCapacity + 100);
            bufferCapacity += 100;
            given_categoriesPtr = given_categories + currentLength;
        }
        *given_categoriesPtr = c;
        given_categoriesPtr++;
        if(c == '\n') escape_before = 1;
    }
    
    fclose(fp);
    number_of_categories++; /*because we have n-1 escape characters in respect to lines*/
    return number_of_categories;
}


FILE *global_orders_file;
int is_done;

order_t *get_current_order(){
    
    char *delims = "|\n\r";
    double price = 0;
    char *idnum;
    size_t lineptr_length = 0;
    char *category;
    char *lineptr = NULL;
    char *title;
    order_t *order = NULL;
    
    if(getline(&lineptr, &lineptr_length, global_orders_file) != EOF){
        title = strtok(lineptr, delims);
        price = atof(strtok(NULL, delims));
        idnum = strtok(NULL, delims);
        category = strtok(NULL, delims);
        
        if (strstr(given_categories, category) == NULL){ /*can be potential bug here*/
            printf("The category %s is not in the list of given categories. Order is not processed.\n", category);
            return NULL;
        }
        
        order = order_create(title, price, idnum, category);
        free(lineptr);
    }else{
        is_done = 1;
        set_isdone_private_globally();
        if(lineptr)free(lineptr);
    }
    return order;
}

void set_isdone_private_globally(){
    int i = 0;
    for(i = 0; i < number_of_categories; i++){
        pthread_mutex_lock(&global_array_of_queues[i]->mutex);
        global_array_of_queues[i]->isdone_private = 1;
        pthread_mutex_unlock(&global_array_of_queues[i]->mutex);
    }
}

void print_final_report(){
    customer_t *customer = NULL;
    for(customer = global_customer_hash;  customer != NULL; customer = customer->hh.next) {
        printf("\n=== BEGIN CUSTOMER INFO ===\n");
        print_balance(customer);
        print_suc_orders(customer->suc_order_list);
        print_failed_orders(customer->fail_order_list);
        printf("\n=== END CUSTOMER INFO ===\n");
    }
}

void print_balance(customer_t *customer){
    printf("### BALANCE ###\n");
    printf("Customer name:%s\n", customer->name);
    printf("Customer ID number:%s\n", customer->idnum);
    printf("Remaining credit balance after all purchases (a dollar amount):%0.2f\n", customer->creditLimit);
}

void print_suc_orders(struct suc_order *order){
    printf("### SUCCESSFUL ORDERS ###\n");
    while(order != NULL && order->bookTitle){
        printf("%s | %0.2f | %0.2f\n", order->bookTitle, order->bookPrice, order->remainingBalance);
        order = order->next;
    }
}

void print_failed_orders(struct fail_order *order){
    printf("### REJECTED ORDERS ###\n");
    while(order != NULL && order->bookTitle){
        printf("%s | %0.2f\n", order->bookTitle, order->bookPrice);
        order = order->next;
    }
}
