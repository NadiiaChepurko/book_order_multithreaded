#include "order.h"

order_t *order_create(char *title, double price, char *idnum, char *category) {
    order_t *order = calloc(1, sizeof(struct order));
    
    if(order == NULL){
        perror("calloc failed. \n");
        exit(-1);
    }
    
    order->idnum = calloc(strlen(idnum) + 1, 1);
    strcpy(order->idnum, idnum);
    
    order->price = price;
    
    order->title = calloc(strlen(title) + 1, 1);
    order->category = calloc(strlen(category) + 1, 1);
    strcpy(order->title, title);
    strcpy(order->category, category);
    
    return order;
}

void order_destroy(order_t *order) {
    if (order) {
        free(order->title);
        free(order->idnum);
        free(order->category);
        free(order);
    }
}