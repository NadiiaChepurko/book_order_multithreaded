#ifndef __book_order__queue_node__
#define __book_order__queue_node__

#include <stdio.h>
#include <stdlib.h>
#include "order.h"

typedef struct queue_node {
    struct order *order;
    struct queue_node *next;
} queue_node_t;

/*
 * Create new node for a queue
 */
queue_node_t *queue_node_create(struct order *, queue_node_t *);

#endif
