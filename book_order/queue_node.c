#include "queue_node.h"

queue_node_t *queue_node_create(order_t *order, queue_node_t *next) {
    
    queue_node_t *new_node = calloc(1, sizeof(queue_node_t));
    
    if(!new_node) {
        perror("Calloc failed. \n");
        exit(-1);
    }
    /*copy order data so order passed as an argument could be freed in the calling function*/
    order_t *new_order = calloc(1, sizeof(order_t));
    new_order->title = calloc(strlen(order->title)+1, 1);
    new_order->title = strcpy(new_order->title, order->title);
    
    new_order->price = order->price;

    new_order->idnum = calloc(strlen(order->idnum)+1, 1);
    new_order->idnum = strcpy(new_order->idnum, order->idnum);
    
    new_order->category = calloc(strlen(order->category)+1, 1);
    new_order->category = strcpy(new_order->category, order->category);
    
    new_node->order = new_order;
    new_node->next = next;
    
    if(order)order_destroy(order);
    
    return new_node;
}
