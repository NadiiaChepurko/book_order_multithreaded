#include <stdio.h>
#include "library.h"

#define LIMIT 3

/*global shared data structures*/
struct Customer *global_customer_hash = NULL;

/*revenue from all successful order*/
int revenue;        //<<<------ do we need it???/?

/*extern variables that is shared between one threads withing multiple source files*/
extern FILE *global_orders_file;
extern int is_done;
extern queue_t **global_array_of_queues;
extern int number_of_categories;

/*pointer to a given categories char string*/
extern char *given_categories;

/*producer thread*/
void *producer_thread(void * filename);

/*consumer thread*/
void *consumer_thread(void * arg);/*accepts queue[i]*/

int main(int argc, const char * argv[]) {
    if (argc != 4) {
        fprintf(stderr, "Error: wrong number of arguments\n");
        exit(-1);
    }
    FILE *fp = fopen(argv[1], "r");
    if(fp == NULL) {
        perror("Error opening file");
        exit(-1);
    }
    
    fill_customers(fp);
    fclose(fp);
    
    /*process arguments*/
    char *bookordersFilename = calloc(strlen(argv[2]), 1);
    bookordersFilename = strcpy(bookordersFilename, argv[2]);
    char *categoriesFilename = calloc(strlen(argv[3]), 1);
    categoriesFilename = strcpy(categoriesFilename, argv[3]);
    
    /*process categories*/
    number_of_categories = get_categories(categoriesFilename);
    if(given_categories == NULL){
        printf("Categories are not specified. Abort.");
        exit(-1);
    }
    
    /*array of pointer to queues*/
    queue_t **queue_arr = calloc(number_of_categories, sizeof(queue_t *));
    global_array_of_queues = queue_arr;
    int i = 0;
    for(i = 0; i < number_of_categories; i++){
        queue_arr[i] = queue_create();
    }
    assign_categories_for_queues();
    
    pthread_t tid[number_of_categories + 1];
    
    if ( pthread_create(&tid[0], NULL, producer_thread, (void *)bookordersFilename) != 0){ /*our producer*/
        perror("pthread_create for producer failed");
        exit(-1);
    };
    
    i = 0;
    char *categoriesPtr = given_categories;
    char *category;
    char *categoryPtr;
    
    /*create consumer thread for each category*/
    for (i = 0; i < number_of_categories; i++) {
        category = calloc(strlen(given_categories), 1);
        categoryPtr = category;
        while(*categoriesPtr != '\n' && *categoriesPtr != '\0'){
            *categoryPtr = *categoriesPtr;
            categoriesPtr++;
            categoryPtr++;
        }
        if(*categoriesPtr != '\0' && *categoriesPtr == '\n'){
            categoriesPtr++;
        }
        
        if (pthread_create(&tid[i+1], NULL, consumer_thread, (void *)queue_arr[i]) != 0 ){/*our consumers*/
            perror("pthread_create for consumer failed");
            exit(-1);
        };
        free(category);
    }
    
    /*wait for all threads to finish before continuing*/
    for (i = 0; i < number_of_categories + 1; i++) {
        pthread_join(tid[i], NULL);
    }
    
    print_final_report();
    
    for(i = 0; i < number_of_categories; i++){
        queue_destroy(global_array_of_queues[i]);
    }
    free(global_array_of_queues);
    free_customers(global_customer_hash);
    free(given_categories);
    free(categoriesFilename);
    free(bookordersFilename);
    
    //pthread_exit(0);
    
    return 0;
}


void *producer_thread(void * filename){
    global_orders_file = fopen((char*)filename, "r");
    if (global_orders_file == NULL) {
        fprintf(stderr, "An error occurred on line %d while opening the file %s.\n",__LINE__, __FILE__);
        exit(-1);
    }

    order_t *current_order = NULL;
    queue_t *category_queue = NULL;
    
    while(1){
        current_order = get_current_order();
        if(current_order == NULL && !is_done) continue; /*might be wrong category*/
        if(is_done && current_order == NULL) {
            int i = 0;
            printf("\n\n --->> NO MORE ORDERES TO PROCESS. BROADCASTING TO ALL CONSUMERS <<--- \n\n");
            for(i = 0; i < number_of_categories; i++)
                pthread_cond_broadcast(&global_array_of_queues[i]->wait_for_not_empty_queue);
            break; /*we have proccessed all orderes*/
        }
        
        category_queue = get_queue_for_category(current_order->category);
        
        /*check that customer id and category for current order is valid*/
        customer_t *customer_for_order = NULL;
        HASH_FIND_STR(global_customer_hash,  current_order->idnum, customer_for_order);
        if (!customer_for_order) { /*we do not have this customer in our database*/
            printf("Customer with %s id is not in our database, order will not be processed\n", current_order->idnum);
            order_destroy(current_order);
            continue;
        }
        if(category_queue == NULL){
            printf("Given category  %s is not assign to any customer for book: %s and customer id: %s", current_order->category, current_order->title, current_order->idnum);
            order_destroy(current_order);
            continue;
        }
        
        /*protect access to inconsistent data*/
        pthread_mutex_lock(&category_queue->mutex);
        
        while(category_queue->size >= LIMIT){
            pthread_cond_signal(&category_queue->wait_for_not_empty_queue);
            printf("Producer waits because queue is full for category %s. \n", category_queue->category);
            pthread_cond_wait(&category_queue->wait_for_empty_queue, &category_queue->mutex);
        }
        printf("\nProducer resumes and enqueues order, Title: %s Category: %s \n\n", current_order->title, current_order->category);
        queue_enqueue(category_queue, current_order);
        category_queue->size++;
        pthread_mutex_unlock(&category_queue->mutex);
    }
    fclose(global_orders_file);
    return NULL;
}


void *consumer_thread(void * arg){
    queue_t *category_queue = (queue_t *) arg;
    
    customer_t *customer = NULL;
    order_t *order = NULL;
    
    while(1){
        pthread_mutex_lock(&category_queue->mutex);
        
        printf("\nConsumer thread for category %s starts working\n", category_queue->category);
        
        while(!category_queue->isdone_private && queue_isempty(category_queue)) { /*tell producer to fill in queue and wait for queue to be full*/
            pthread_cond_signal(&category_queue->wait_for_empty_queue);
            printf("\nConsumer for category %s waits because queues or buffers are empty.\n", category_queue->category);
            pthread_cond_wait(&category_queue->wait_for_not_empty_queue, &category_queue->mutex);
        }
        if (category_queue->isdone_private && queue_isempty(category_queue)) { /*we processed all orderes from this category queue*/
            pthread_mutex_unlock(&category_queue->mutex);
            printf("\n\n--->> CONSUMER THREAD FOR CATEGORY %s IS DONE. BYE-BYE <<--\n\n", category_queue->category);
            return NULL;
        }
        
        order = queue_dequeue(category_queue);
        
        printf("\nConsumer thread for category %s resumes and got order. \n\tTitle: %s \n\tCategory:%s \n\n", category_queue->category, order->title, order->category);
        
        category_queue->size--;
        HASH_FIND_STR(global_customer_hash, order->idnum, customer);
        if (!customer) { /*we do not have this customer in our database*/
            printf("Customer for with %s id is not in our database, order will not be processed", order->idnum);
            continue;
        }
        
        if (customer->creditLimit < order->price) { /*customer does not have enough money to pay for this order*/
            printf("Funds of %s are below the required sum. \n\tBook order: %s cannot be processed \n\tRemaining credit: $%.2f \n \n", customer->name, order->title, customer->creditLimit);
            add_fail_order(customer, order->title, order->price);
        }
        else { /*customer has enough funds*/
            customer->creditLimit -= order->price;
            revenue +=order->price;
            printf("Purchase is made.\n"
                   "\tBook: %s\n\tPrice: $%.2f\n"
                   "\tShipping info: %s\n\n", order->title, order->price, customer->address);
            add_successfull_order(customer, order->title, order->price, customer->creditLimit);
        }
        if(order)order_destroy(order);
        pthread_mutex_unlock(&category_queue->mutex);
        
    }
    
    return NULL;
}