#include "customer.h"


void addCustomer(struct Customer *customerStruct){
    struct Customer *newCustomer = NULL;
    HASH_FIND_STR(global_customer_hash, customerStruct -> idnum, newCustomer);
    if(newCustomer == NULL){
        newCustomer = calloc(1, sizeof(struct Customer));
        newCustomer->name = customerStruct->name;
        newCustomer->idnum = customerStruct->idnum;
        newCustomer->address = customerStruct->address;
        newCustomer->creditLimit = customerStruct->creditLimit;
        newCustomer->suc_order_list = calloc(1, sizeof(struct suc_order));
        newCustomer->suc_order_list->next = NULL;
        newCustomer->fail_order_list = calloc(1, sizeof(struct fail_order));
        newCustomer->fail_order_list->next = NULL;
        HASH_ADD_KEYPTR(hh, global_customer_hash, newCustomer->idnum, strlen(newCustomer->idnum), newCustomer);
    }else{
        printf("Customer id is not unique.");
        exit(-1);
    }
    
}

void print_customers(){
    struct Customer *temp = NULL;
    for(temp = global_customer_hash; temp != NULL; temp = temp -> hh.next){
        printf("Name: %s \n", temp->name);
        printf("Id: %s \n", temp->idnum);
        printf("Credit: %f \n", temp->creditLimit);
        printf("Address: %s \n", temp->address);
        printf("\n");
    }
}

void free_customers(struct Customer *cp){
    struct Customer *current, *temp;
    HASH_ITER(hh, global_customer_hash, current, temp)
    {
        free(current->address);
        free(current->idnum);
        free(current->name);
        freeFailedOrders(current->fail_order_list);
        freeSuccessfulOrders(current->suc_order_list);
        HASH_DEL(global_customer_hash, current);
        free(current);
    }
}
void add_fail_order(struct Customer *customer, char *bookTitle, double bookPrice){
    struct fail_order *fail_order = calloc(1, sizeof(struct fail_order));
    fail_order->bookTitle = calloc(strlen(bookTitle) + 1, 1);
    fail_order->bookTitle = strcpy(fail_order->bookTitle, bookTitle);
    fail_order->bookPrice = bookPrice;
    fail_order->next = customer->fail_order_list;
    customer->fail_order_list = fail_order;
}

void add_successfull_order(struct Customer *customer, char *bookTitle, double bookPrice, double remainingBalance){
    struct suc_order *suc_order = calloc(1, sizeof(struct suc_order));
    suc_order->bookTitle = calloc(strlen(bookTitle) + 1, 1);
    suc_order->bookTitle = strcpy(suc_order->bookTitle, bookTitle);
    suc_order->bookPrice = bookPrice;
    suc_order->remainingBalance = remainingBalance;
    suc_order->next = customer->suc_order_list;
    customer->suc_order_list = suc_order;
}

void freeSuccessfulOrders(struct suc_order *front){
    struct suc_order *temp;
    
    while(front != NULL && front->bookTitle != NULL){
        temp = front;
        front = front->next;
        free(temp->bookTitle);
        free(temp);
    }
    free(front);
}

void freeFailedOrders(struct fail_order *front){
    struct fail_order *temp;
    
    while(front != NULL && front->bookTitle != NULL){
        temp = front;
        front = front->next;
        free(temp->bookTitle);
        free(temp);
    }
    free(front);
}

void fill_customers(FILE* fp){
    char *buffer;
    char *bufferPtr;
    int bufferCapacity = 1000;
    int currentLength = 0;
    int c = 1;
    double creditLimit = 0;
    struct Customer *customer;
    int check = 0;
    
    while((c = getc(fp)) != EOF){
        if(c == 34){ // if "
            buffer = calloc(1000, 1);
            bufferPtr = buffer;
            while((c = getc(fp)) != 34){
                
                if(currentLength + 1 == bufferCapacity){
                    buffer = realloc(buffer, bufferCapacity + 100);
                    bufferCapacity += 100;
                    bufferPtr = buffer + currentLength;
                }
                *bufferPtr = c;
                bufferPtr++;
                currentLength++;
            }/*here we are done with the name*/
            
            customer = calloc(1, sizeof(struct Customer));
            customer->name = calloc(currentLength + 1, 1);
            customer->name = strcpy(customer->name, buffer);
            free(buffer);
            currentLength = 0;
            bufferCapacity = 1000;
            
            c = getc(fp); //skip '|'
            
            buffer = calloc(1000, 1);
            bufferPtr = buffer;
            
            while(isdigit((c = getc(fp)))){
                if(currentLength + 1 == bufferCapacity){
                    buffer = realloc(buffer, bufferCapacity + 100);
                    bufferCapacity += 100;
                    bufferPtr = buffer + currentLength;
                }
                *bufferPtr = c;
                bufferPtr++;
                currentLength++;
            }
            
            customer->idnum = calloc(currentLength + 1, 1);
            customer->idnum = strcpy(customer->idnum, buffer);
            free(buffer);
            currentLength = 0;
            bufferCapacity = 1000;
            buffer = calloc(1000, 1);
            bufferPtr = buffer;
            
            while(isdigit((c = getc(fp))) || c == '.'){
                if(currentLength + 1 == bufferCapacity){
                    buffer = realloc(buffer, bufferCapacity + 100);
                    bufferCapacity += 100;
                    bufferPtr = buffer + currentLength;
                }
                *bufferPtr = c;
                bufferPtr++;
                currentLength++;
            }
            
            creditLimit = atof(buffer);
            customer->creditLimit = creditLimit;
            free(buffer);
            currentLength = 0;
            bufferCapacity = 1000;

            buffer = calloc(1000, 1);
            bufferPtr = buffer;
            
            while((c = getc(fp)) != 10){
                if(currentLength + 1 == bufferCapacity){
                    buffer = realloc(buffer, bufferCapacity + 100);
                    bufferCapacity += 100;
                    bufferPtr = buffer + currentLength;
                }
                *bufferPtr = c;
                bufferPtr++;
                currentLength++;
            }
            
            customer->address = calloc(currentLength + 1, 1);
            customer->address = strcpy(customer->address, buffer);
            free(buffer);
            currentLength = 0;
            bufferCapacity = 1000;
            
            addCustomer(customer);
            check = 1;
            free(customer);
        }
        
    }
    
    if(!check){
        printf("Incorrent format or number of customers. Abort.");
        exit(-1);
    }
}

