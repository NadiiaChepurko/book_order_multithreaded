#ifndef __book_order__queue__
#define __book_order__queue__

#include <stdio.h>
#include "order.h"
#include "queue_node.h"
#include <pthread.h>

/*
 * Queue structure for consumers threads
 */
typedef struct queue {
    pthread_mutex_t mutex; /*protect our queue while it is in inconsistent state*/
    pthread_cond_t wait_for_not_empty_queue; /*shout at consumers that wait for queue to have some items*/
    pthread_cond_t wait_for_empty_queue; /*shout at producer that waits for empty queue to start processing orders*/
    char *category;
    int size;
    int isdone_private; /*tells consumer that we should not expect any more orders*/
    struct queue_node *rear;
} queue_t;

/*
 * Creates a new empty queue
 */
queue_t *queue_create(void);

/*Enqueues new node in a queue*/
void queue_enqueue(queue_t *, order_t *);

/*
 * Dequeue node from a queue
 */
order_t *queue_dequeue(queue_t *queue);

/**
 * Determines whether or not the given queue is empty or not.
 */
int queue_isempty(queue_t *);

/**
 * Destroys the queue, freeing all associated memory. Do not call this method if
 * threads are still waiting to perform enequeue or dequeue operations on the
 * specified queue.
 */
void queue_destroy(queue_t *);

/*
 * Get element from a queue and check its data withoit dequeueing it
 */
order_t *queue_peek(queue_t *queue);

#endif
