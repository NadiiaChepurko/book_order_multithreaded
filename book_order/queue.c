#include "queue.h"

/**
 * Creates a new queue, initially empty.
 */
queue_t *queue_create() {
    queue_t *new_queue = calloc(1, sizeof(queue_t));
    if (new_queue == NULL){
        perror("calloc failed to allocate memory. \n");
        exit(-1);
    }
    if (pthread_mutex_init(&new_queue->mutex, NULL) != 0) {
        perror("pthread_mutex_init failed. \n");
        exit(-1);
    }
    if (pthread_cond_init(&new_queue->wait_for_not_empty_queue, NULL) != 0) {
        /* signal for our consumers */
        perror("pthread_cond_init failed. \n");
        exit(-1);
    }
    if (pthread_cond_init(&new_queue->wait_for_empty_queue, NULL) != 0) {
        /* signal for our producer */
        perror("pthread_cond_init failed. \n");
        exit(-1);
    }
    new_queue->size = 0;
    new_queue->isdone_private = 0;
    new_queue->category = NULL;
    new_queue->rear = NULL;
    
    return new_queue;
}

/*
 * Enqueues data
 */
void queue_enqueue(queue_t *queue, order_t *order) {
    
    if (queue == NULL){
        printf("Queue is NULL. Cannot enqueue in it. \n ");
        exit(-1);
    };
    
    queue_node_t *node = queue_node_create(order, NULL);
    
    /*empty queue*/
    if (queue->rear == NULL) {
        queue->rear = node;
        queue->rear->next = queue->rear;
    }else {
        node->next = queue->rear->next;
        queue->rear->next = node;
        queue->rear = node;
    }
}

/*
 * Dequeues data
 */

order_t *queue_dequeue(queue_t *queue) {
    if (queue == NULL || queue->rear == NULL) return NULL;
    
    queue_node_t *dequeued_node;
    order_t *order;
    
    if (queue->rear->next == NULL || queue->rear == queue->rear->next) { /*single node is left*/
        order = queue->rear->order;
        free(queue->rear);
        queue->rear = NULL;
    }else {
        dequeued_node = queue->rear->next;
        order = queue->rear->next->order;
        queue->rear->next = queue->rear->next->next;
        free(dequeued_node);
    }
    return order;
}

/*
 * Frees dynamically allocated memory
 */

void queue_destroy(queue_t *queue){
    queue_node_t *node, *next;
    if (queue) {
        if (queue->rear == NULL){} /*There is no item yet in the queue*/
        else if (queue->rear == queue->rear->next) { /*one on elemenet in the queue*/
            order_destroy(queue->rear->order);
            free(queue->rear);
        }else{
            node = queue->rear;
            while(node) {
                next = node->next;
                order_destroy(node->order);
                free(node);
                node = next;
            }
        }
        pthread_mutex_destroy(&queue->mutex);
        pthread_cond_destroy(&queue->wait_for_not_empty_queue);
        pthread_cond_destroy(&queue->wait_for_empty_queue);
        free(queue->category);
        free(queue);
    }
}

int queue_isempty(queue_t *queue) {
    return queue->rear == NULL || queue == NULL;
}

order_t *queue_peek(queue_t *queue) {
    if (queue && queue->rear && queue->rear->next) {
        return queue->rear->next->order;
    }
    return NULL;
}