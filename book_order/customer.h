#ifndef __book_order__customer__
#define __book_order__customer__

#include <stdio.h>
#include <stdlib.h>
#include "uthash.h"
#include <string.h>
#include <ctype.h>

typedef struct Customer{
    char* name;
    char* idnum;
    double creditLimit;
    char *address;
    struct fail_order *fail_order_list;
    struct suc_order *suc_order_list;
    UT_hash_handle hh;
}customer_t;

/*Hash Table that holds cutomer info*/
extern customer_t *global_customer_hash;

struct suc_order{
    char *bookTitle;
    double bookPrice;
    double remainingBalance;
    struct suc_order *next;
};

struct fail_order{
    char *bookTitle;
    double bookPrice;
    struct fail_order *next;
};

void fill_customers(FILE*);
void addCustomer(customer_t *);
void add_fail_order(customer_t *customer, char *, double);
void add_successfull_order(customer_t *customer, char *, double, double);

/*function to free heap memory*/
void free_customers(customer_t *);
void freeSuccessfulOrders(struct suc_order *);
void freeFailedOrders(struct fail_order *);

/*free helper test methods*/
void print_customers();

#endif
